# PX4 Autopilot Project



This project helps in adding new modules and creating new customized parameters for PX4 source code.

Follow the AddNewModule.pdf file to add a new module.

Download Add Parameter folder and run the AddParameter.mlx file to add new parameters.

function folder = AddModule()
    folder = uigetdir();
    if folder ~= 0
        disp(folder);
    else
        disp("No folder was selected.");
    end
end
classdef IntParameter < Parameter
    methods
        function obj = IntParameter(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue)
            obj@Parameter(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue);
            add_parameter(obj);
        end
        
        function newParams = get_new_params_cFile(obj)
            % Define the new parameter string
            newParams = ['PARAM_DEFINE_INT32(' obj.Name ', ' num2str(obj.DefaultValue) ');'];
        end
        
        function newParams = get_new_params_hFile(obj)
            newParams = ['(ParamInt<px4::params::' obj.Name '>) '];
        end

        function check = type_check(obj)
            check = 0;
            variable_names = {'MinValue', 'MaxValue', 'DefaultValue'};
            for i = 1:numel(variable_names)
                if rem(str2double(obj.(variable_names{i})),1) ~= 0
                    warning((variable_names{i}) + " should be an Integer!");
                    check = -1;
                end
            end
        end
    end
end
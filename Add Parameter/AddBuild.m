function AddBuild(ModuleAddress, Type, Name, Group, ShortDescription, DetailDescription, MinValue, MaxValue, Unit, Category, DefaultValue, addbuild)
    if addbuild == 1
        AddInfo(ModuleAddress, Type, Name, Group, ShortDescription, DetailDescription, MinValue, MaxValue, Unit, Category, DefaultValue);
    end

    system([fullfile(codertarget.pixhawk.internal.getPX4CygwinDir,'run-console_Simulink.bat'),' "',...
    'cd ',strrep(fullfile(px4.internal.util.CommonUtility.getPX4FirmwareDir,'Firmware'),'\','/'),'; ',...
    'make px4_sitl none', '"&']);

    disp("Build in progress ...");
end
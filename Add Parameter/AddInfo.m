classdef AddInfo < Parameter
    methods
        function obj = AddInfo(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue)
            obj@Parameter(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue);
            ParaType(obj);
        end
    end
end
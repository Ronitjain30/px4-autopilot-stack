classdef Parameter
    properties
        module_address
        type
        Name
        Group
        one_liner
        description
        MinValue
        MaxValue
        unit
        category
        DefaultValue
    end
    
    methods
        function obj = Parameter(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue)
            obj.module_address = module_address;
            obj.type = type;
            obj.Name = Name;
            obj.Group = Group;
            obj.one_liner = one_liner;
            obj.description = description;
            obj.MinValue = MinValue;
            obj.MaxValue = MaxValue;
            obj.unit = unit;
            obj.category = category;
            obj.DefaultValue = DefaultValue;
        end


        function ParaType(obj)
            if obj.type == "INT"
                IntParameter(obj.module_address, obj.type, obj.Name, obj.Group, obj.one_liner, obj.description, obj.MinValue, obj.MaxValue, obj.unit, obj.category, obj.DefaultValue);
            else
                FloatParameter(obj.module_address, obj.type, obj.Name, obj.Group, obj.one_liner, obj.description, obj.MinValue, obj.MaxValue, obj.unit, obj.category, obj.DefaultValue);
            end
        end
        
        function add_parameter(obj)
            % Validation checks
            check = obj.type_check();
            valid = obj.validation_checks();
            if valid == -1 || check == -1
                return;
            end

            % Open and read the contents of the .c file and close it.
            module = extractAfter(obj.module_address, '\modules\');
            fid = fopen([obj.module_address '\' module '_params.c'], 'r');
            data = fread(fid, 'uint8=>char')';
            fclose(fid);

            % Define the new parameter string
            newParams = obj.get_new_params_cFile();
            
            % Change file extension to .txt and add some data
            new_file_name = "temp.txt";
            new_data = [data, newline, obj.generate_parameter_string(), newParams];
            fid = fopen(new_file_name, 'w');
            
            % Write the modified text to the temporary file
            fwrite(fid, new_data, 'char');
            fclose(fid);

            % Change file extension back to .c
            movefile(new_file_name, [obj.module_address '\' module '_params.c']);

            % Open the .h file in read mode and read the file contents as a character array
            fileID = fopen([obj.module_address '\' module '.h'], 'r');
            fileText = char(fread(fileID, '*char')');
            
            % Find the index of the start of the DEFINE_PARAMETERS section
            startStr = 'DEFINE_PARAMETERS(';
            startIndex = strfind(fileText, startStr) + length(startStr);
    
            % Define the new parameter as a string
            space = sprintf('\t\t');
            newParams = obj.get_new_params_hFile();
            newParams = [space newParams '_param_' lower(obj.Name) ','];
            
            % Insert the new parameter
            newText = [fileText(1:startIndex) newParams newline fileText(startIndex+1:end)];
    
            % Open the file in write mode, Write the modified text to the file and close it
            fileID = fopen([obj.module_address '\' module '.h'], 'w');
            fwrite(fileID, newText, 'char');
            fclose(fileID);

            disp(obj.category);
            disp("Parameter Added!!" + newline);
        end
        
        function newParams = get_new_params_cFile(obj)
            % This method is overridden by child classes
        end
        
        function newParams = get_new_params_hFile(obj)
            % This method is overridden by child classes
        end

        function check = type_check(obj)
            % This method is overridden by child classes
        end
        
        function parameter_string = generate_parameter_string(obj)
            % Generate the parameter string using the object properties
            parameter_string = ['/**', newline, ' * ', obj.one_liner, newline, ' * ', newline, ' * ', obj.description, newline, ' * ', newline];
            
            % Add the unit field if not empty
            if ~isempty(obj.unit)
                parameter_string = [parameter_string, ' * @unit ', obj.unit, newline];
            end

            parameter_string = [parameter_string, ' * @min ', obj.MinValue, newline, ' * @max ', obj.MaxValue, newline, ' * @group ', obj.Group, newline];
            
            % Add the category field if not empty
            if ~isempty(obj.category)
                parameter_string = [parameter_string, ' * @category ', obj.category, newline];
            end
            
            parameter_string = [parameter_string, ' */', newline];
        end

        function valid = validation_checks(obj)
            valid = 0;

            % Required length for Parameter Name and of Short Description cannot exceed 16 and 70 respectively.
            if length(obj.Name) > 16
                warning("Size of Name cannot be more than 16!");
                valid = -1;
                
            end
            if length(obj.one_liner) > 70
                warning("Size of Short Description cannot be more than 70!");
                valid = -1;
            end

            % Required fields should not be empty
            variable_names = {'Name', 'Group', 'MinValue', 'MaxValue', 'DefaultValue'};
            for i = 1:numel(variable_names)
                if isempty(obj.(variable_names{i}))
                    warning((variable_names{i}) + " cannot be empty!");
                    valid = -1;
                end
            end

            % Default Value should be between minimum and maximum value
            min_val = str2double(obj.MinValue);
            max_val = str2double(obj.MaxValue);
            def_val = str2double(obj.DefaultValue);
            if def_val < min_val || def_val > max_val
                warning("Default Value should lie between " + min_val + " and " + max_val + "!");
                valid = -1;
            end

            % Check if the unit is allowed
            allowed_units = {'%', 'Hz', 'mAh', 'rad', '%/rad', 'rad/s', 'rad/s^2', '%/rad/s',  'rad s^2/m','rad s/m', 'bit/s', 'B/s', ...
                'deg', 'deg*1e7', 'deg/s', 'celcius', 'gauss', 'gauss/s', 'mgauss', 'mgauss^2', 'hPa', 'kg', 'kg/m^2', 'kg m^2', ...
                'mm', 'm', 'm/s', 'm^2', 'm/s^2', 'm/s^3', 'm/s^2/sqrt(Hz)', 'm/s/rad', 'Ohm', 'V', 'us', 'ms', 's', 'S', 'A/%', ...
                '(m/s^2)^2',  'm/m',  'tan(rad)^2', '(m/s)^2', 'm/rad', 'm/s^3/sqrt(Hz)', 'm/s/sqrt(Hz)', 's/(1000*PWM)', '%m/s', ...
                'min', 'us/C', 'N/(m/s)', 'Nm/(rad/s)', 'Nm', 'N', 'normalized_thrust/s', 'normalized_thrust', 'norm', 'SD', ''};
            if ~any(strcmp(obj.unit, allowed_units))
                warning("This unit does not exist, refer to the documentaion to get the allowed set of Units.");
                valid = -1;
            end

            % Check if the category exists
            if isempty(obj.category) || obj.category == "Developer" || obj.category == "System"
            else
                warning("Categories can only be Standard (default), Developer or System!" + newline + "Leave empty for Standard.");
                valid = -1;
            end
        end

    end
end
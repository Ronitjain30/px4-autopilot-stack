classdef FloatParameter < Parameter
    methods
        function obj = FloatParameter(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue)
            obj@Parameter(module_address, type, Name, Group, one_liner, description, MinValue, MaxValue, unit, category, DefaultValue);
            add_parameter(obj);
        end
        
        function newParams = get_new_params_cFile(obj)
            % Define the new parameter string
            newParams = ['PARAM_DEFINE_FLOAT(' obj.Name ', ' num2str(obj.DefaultValue) ');'];
        end
        
        function newParams = get_new_params_hFile(obj)
            newParams = ['(ParamFloat<px4::params::' obj.Name '>) '];
        end

        function check = type_check(obj)
            check = 0;
        end
    end
end